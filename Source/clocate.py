import curses as c
from time import sleep # dev

class Interface:

    SEARCH, BROWSE = range(2)

    # Prompt Settings
    prompt_t = "locate: "
    prompt_h = 3
    prompt_p = 3
    prompt_s = c.A_REVERSE

    # Results Settings
    result_p = 3
    result_i = 0

    # Cursor Settings
    cursor_s = c.A_BLINK | c.A_REVERSE
    cursor_t = '_'

    # Scrollbar Settings
    scroll_w = 1
    scroll_pos = 0

    # dev
    results = ['A', 'B', 'C', 'D']

    def __init__(self):
        # Initialise Curses
        self.scr = c.initscr()
        c.noecho()
        c.cbreak()
        self.scr.keypad(True)
        self.scr.clear()
        self.H, self.W  = self.scr.getmaxyx()
        # Initialise Variables
        self.term = ""
        self.mode = self.SEARCH

    def drawPrompt(self):
        s = self.prompt_t + self.term
        self.scr.addstr(0, 0, " "*self.W, self.prompt_s)
        self.scr.addstr(1, 0, " "*self.W, self.prompt_s)
        self.scr.addstr(1, self.prompt_p + 1, s, self.prompt_s)
        if self.mode is self.SEARCH:
            self.scr.addstr(1, self.prompt_p + 1 + len(s), self.cursor_t, self.cursor_s)
        self.scr.addstr(2, 0, " "*self.W, self.prompt_s)

    def minimisePath(self, path, limit):
        p = path.split('/')
        f = p[-1]
        p = p[:-1]
        s = '/'.join(p) + '/' + f
        n = 0
        while len(s) > limit and n < len(path):
            p[n] = p[n][0]
            s = '/'.join(p) + '/' + f
            n += 1
        if len(s) > limit:
            s = s[-limit:]
        return s

    def drawResults(self):
        r = []
        for path in self.results:
            r.append(self.minimisePath(path, self.W - self.scroll_w - self.result_p - 1))
        draw_to = min([self.H - self.prompt_h, len(r) + 1])
        style = 0
        for y in range(0, draw_to - 1):
            if y is self.result_i and self.mode is self.BROWSE:
                style = c.A_REVERSE
            else:
                style = 0
            self.scr.addstr(y+1+self.prompt_h, self.result_p, r[y+self.scroll_pos], style)


    def run(self):
        try:
            while True:
                self.drawPrompt()
                self.drawResults()
                char = self.scr.getch()
                self.scr.addstr(10, 0, f"Selected {self.result_i}")
                self.scr.addstr(11, 0, f"Char {char}")
                if self.mode is self.SEARCH:
                    if char is 127:
                        self.term = self.term[:-1]
                        self.scr.addstr(5, 0, f"Backspace! {self.term}")
                    if char is 27:
                        break
                    if char is 10:
                        self.mode = self.BROWSE
                    else:
                        self.term += str(char)
                if self.mode is self.BROWSE:
                    if char is 27:
                        self.mode = self.SEARCH
                    if char is 258:
                        self.result_i += 1
                        if self.result_i > len(self.results):
                            self.result_i = len(self.result_i)
                    if char is 259:
                        self.result_i -= 1
                        if self.result_i < 0:
                            self.result_i = 0
                    
            # self.drawResults([
                # "My/Path/filename.txt",
            #     "My/Path/Really/Really/Super/Long/Path/filename.png",
            #     "My/Path/filename.file",
            #     "My/Path/filename.md",
            #     "My/Path/filename.c",
            # ])
                self.scr.refresh()
        finally:
            self.destroy()

    def destroy(self):
        c.nocbreak()
        self.scr.keypad(False)
        c.echo()
        c.endwin()
        self.scr.clear()

Interface().run()